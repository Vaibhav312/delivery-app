import 'package:flutter/material.dart';

abstract class GlobalStyles{
  static const Color primaryColor = Colors.black;
  static const Color secondaryColor = Colors.blueGrey;
  static const Color dividerColor = Colors.grey;
  static const Color headingColor = Colors.black;
  static const Color iconColor = Colors.redAccent;


}