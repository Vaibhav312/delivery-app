import 'package:first_app/screens/Order/styles/global_styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Event {
  Event({this.eventName, this.description});
  final String eventName;
  final String description;
}

class EventCard extends StatelessWidget {
  int data;
  EventCard(int i){
    this.data=i;
    //print(this.data);
  }

  List<Event> eventsList = [
    Event(eventName: 'Transformation Series 2020', description: 'Connaught Place'),
    Event(eventName: 'Comedy Club of India 2020', description: 'Connaught Place'),
    Event(eventName: 'Transformation Series 2020', description: 'Delhi'),
    Event(eventName: 'Comedy Club of India 2020', description: 'Agra'),
    Event(eventName: 'Transformation Series 2020', description: 'Mumbai'),
    Event(eventName: 'Comedy Club of India 2020', description: 'Haryana'),
    Event(eventName: 'Transformation Series 2020', description: 'Gurgaon'),
    Event(eventName: 'Comedy Club of India 2020', description: 'Jaipur'),

  ];



  @override
  Widget build(BuildContext context) {
    return Container(
      height: 160.0,
      child: ListView.separated(
          itemCount: (data==2) ? 2 : eventsList.length,
          itemBuilder: (context, index) {
            return  ListTile(
              title: Text(eventsList[index].eventName,
                style: TextStyle(
                    fontSize: 15.0,
                    color: GlobalStyles.secondaryColor,
                    fontWeight: FontWeight.bold,
                ),
              ),
              leading: Icon(Icons.event_available,size: 50.0,color: GlobalStyles.iconColor),
              subtitle: Text(eventsList[index].description,
                style: TextStyle(
                  fontSize: 15.0,
                  color: GlobalStyles.secondaryColor,
                ),
              ),
              trailing: Icon(Icons.keyboard_arrow_right,
                size: 30.0,
                color: GlobalStyles.secondaryColor,
              ),
            );
          },
        separatorBuilder: (context,index){
            return Divider(
              thickness: 2.0,
              indent: 15,
              endIndent: 15,
              color: GlobalStyles.dividerColor,
            );
        },

      ),
    );
  }

}


