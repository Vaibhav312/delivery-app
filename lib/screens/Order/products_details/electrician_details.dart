import 'package:first_app/screens/Order/custom_widgets/basicPadding.dart';
import 'package:first_app/screens/Order/styles/global_styles.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Electrician {
  final String name;
  final String details;
  final String phoneNumber;
  final String fee;
  final String recommended;

  Electrician(
          {
            this.name,
            this.details,
            this.phoneNumber,
            this.fee,
            this.recommended
          }
      );

}

class ElectricianDetails extends StatefulWidget {
  @override
  _ElectricianDetailsState createState() => _ElectricianDetailsState();
}

class _ElectricianDetailsState extends State<ElectricianDetails> {

  List<Electrician> electricianList = [
    Electrician(name: 'Electricians in Delhi', details: 'Rampal Chowk, Dwarka Sector 7, Delhi - 110075',phoneNumber: '12345678',fee: '250',recommended: 'true'),
    Electrician(name: 'Om Electricals', details: 'Shop Number 50/2, Dhankot, Gurgaon - 122001, Near Dwarka Expressway, Near Baba Shayam Mandir.',phoneNumber: '12345678',fee: '250',recommended: 'false'),
    Electrician(name: 'Friends Power Solutions', details: '93A, Meharchand Market, Lodhi Road, Delhi - 110003, Near Police Station',phoneNumber: '12345678',fee: '250',recommended: 'true'),
    Electrician(name: 'Right Handy', details: 'Huda Market, Gurgaaon',phoneNumber: '12345678',fee: '250',recommended: 'false'),
    Electrician(name: 'A One Solutions', details: 'Gali No 1, Kapashera, Delhi',phoneNumber: '12345678',fee: '250',recommended: 'false'),
    Electrician(name: 'S R Electrical Works', details: 'Dharam Mandir',phoneNumber: '12345678',fee: '250',recommended: 'true'),
    Electrician(name: 'Kunal Electrician', details: 'Block D-95, Noida',phoneNumber: '12345678',fee: '250',recommended: 'false'),
    Electrician(name: 'Kalka Electricals', details: 'Near Shiv, Noida Sector 63 ',phoneNumber: '12345678',fee: '250',recommended: 'false'),
    Electrician(name: 'Jkm Construction', details: 'Main Road, Malviya Nagar, Delhi',phoneNumber: '12345678',fee: '250',recommended: 'true'),
    Electrician(name: 'Vinod Electricals And Ac Work', details: 'Laxmi Nagar, Delhi',phoneNumber: '12345678',fee: '250',recommended: 'false'),
    Electrician(name: 'Maa Vaishno Electrical All Service', details: 'Connaught Place',phoneNumber: '12345678',fee: '250',recommended: 'false'),

  ];

  Widget recommend(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        RatingBar(
          initialRating: 3,
          minRating: 1,
          direction: Axis.horizontal,
          allowHalfRating: true,
          itemCount: 5,
          itemSize: 25.0,
          itemBuilder: (context, _) => Icon(
            Icons.star,
            color: Colors.amber,
          ),
          onRatingUpdate: (rating) {
            print(rating);
          },
        ),
        Row(
          children: [
            Icon(Icons.flag,color: GlobalStyles.iconColor),
            Text('Recommended')
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Electricians'),
        centerTitle: true,
      ),
      body: Container(
        margin: EdgeInsets.all(10.0),
          child: ListView.builder(
            itemCount: electricianList.length,
            padding: EdgeInsets.symmetric(vertical: 10.0),
            itemBuilder: (context,index){
              return Card(
                elevation: 5.0,
                child: ListTile(
                  contentPadding: EdgeInsets.symmetric(vertical: 10.0,horizontal: 10.0),
                  leading: Icon(Icons.lightbulb_outline,size: 50.0,color: GlobalStyles.iconColor),
                  title: Text(
                    electricianList[index].name,
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                        color: GlobalStyles.primaryColor,
                      ),
                  ),
                  subtitle: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(electricianList[index].details),
                      BasicPadding(),
                      Text('Rs.${electricianList[index].fee}',
                      style: TextStyle(
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold
                      ),
                      ),
                      BasicPadding(),
                      (electricianList[index].recommended=='true') ? recommend() : BasicPadding(),
                    ],
                  ),
                  trailing: Icon(
                      Icons.call,
                      size: 40.0,
                      color: GlobalStyles.iconColor),
                ),
              );
            },
          ),
      ),
    );
  }
}
